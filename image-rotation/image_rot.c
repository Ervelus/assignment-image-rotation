#include "image_rot.h"
//#include "bmp_tools.h"

struct image rotateImage(struct image src){

    struct image res = createImage(src.height, src.width);

    for (uint32_t row = 0; row < src.height; ++row ){
        for (uint32_t col = 0; col < src.width; ++col ){
            res.data[getResPixelAddressByCoordinates(src, col, row)] = src.data[getSourcePixelAddressByCoordinates(src, col, row)];
        }
    }
    return res;
}
