#ifndef IMAGEROT_IMAGE_ROT_H
#define IMAGEROT_IMAGE_ROT_H
#include "image.h"
#include "utils.h"

struct image rotateImage(struct image src);

#endif //IMAGEROT_IMAGE_ROT_H
