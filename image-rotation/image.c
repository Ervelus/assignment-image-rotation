#include "image.h"

struct image createImage(uint64_t width, uint64_t height){
    return (struct image) {width, height, .data = malloc(sizeof(struct pixel) * width * height)};
}

void destroyImage(struct image* image){
    free(image->data);
    //free(image);
}

