#include "utils.h"

void exitBecauseOfError(const char *msg){
    printf("%s\n", msg);
    exit(1);
}

uint64_t getResPixelAddressByCoordinates(struct image src, uint32_t col, uint32_t row){
    return row + col*src.height;
}

uint64_t getSourcePixelAddress(struct image src, uint32_t col, uint32_t row){
    return col + (src.height - row - 1) * src.width;
}