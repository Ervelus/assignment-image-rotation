#ifndef IMAGEROT_IMAGE_H
#define IMAGEROT_IMAGE_H
#include <stdint.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image createImage(uint64_t width, uint64_t height);
void destroyImage(struct image*);


#endif //IMAGEROT_IMAGE_H
