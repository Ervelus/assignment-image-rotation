#ifndef IMAGEROT_UTILS_H
#define IMAGEROT_UTILS_H
#include "bmp_tools.h"

enum openCloseStatus  {
    OPEN_OK = 0,
    OPEN_ERROR,
    CLOSE_OK,
    CLOSE_ERROR
};

static const char* const readErrorsMessages[] = {
        [READ_INVALID_SIGNATURE] = "Invalid signature",
        [READ_INVALID_BITS] = "Invalid bits",
        [READ_INVALID_HEADER] = "Invalid header",
        [READ_INVALID_FORMAT] = "Invalid format"
};

static const char* const writeErrorMessages[] = {
        [WRITE_ERROR] = "Error while writing to output file"
};

static const char* const openCloseError[] = {
        [OPEN_ERROR] = "Error while opening file",
        [CLOSE_ERROR] = "Error while closing file"
};

void exitBecauseOfError(const char *msg);

uint64_t getResPixelAddressByCoordinates(struct image src, uint32_t col, uint32_t row);
uint64_t getSourcePixelAddressByCoordinates(struct image src, uint32_t col, uint32_t row);

#endif //IMAGEROT_UTILS_H

