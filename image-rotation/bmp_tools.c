#include "bmp_tools.h"

enum readStatus checkBmpHeader(const struct bmp_header header){
    //check if bftype is valid
    if(header.bfType != 0x4d42){
        return READ_INVALID_SIGNATURE;
    }
    //check if image data begins immediately after the header data
    if (header.bOffBits != 54){
        return READ_INVALID_HEADER;
    }
    //check file size
    if ((header.bOffBits + header.biSizeImage) != header.bfileSize){
        return READ_INVALID_HEADER;
    }
    //check the number of planes
    if(header.biPlanes != 1){
        return READ_INVALID_HEADER;
    }
    //check compression
    if (header.biCompression != 0){
        return READ_INVALID_FORMAT;
    }
    //check bits for pixel
    if(header.biBitCount != 24){
        return READ_INVALID_FORMAT;
    }
    //check colors
    if((header.biClrImportant + header.biClrUsed) != 0){
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}


enum readStatus fromBmp(FILE* in, struct image* img ){
    //creating struct for header
    struct bmp_header header = {0};

    //check if header is empty
    if(fread(&header, sizeof (struct bmp_header), 1, in) < 1){
        //free(&header);
        return READ_INVALID_HEADER;
    }

    //check if header is valid
    //if it is not - end
    const enum readStatus status = checkBmpHeader(header);
    if ( status != READ_OK ) {
        //free( &header );
        return status;
    }

    //creating img based on header read
    *img = createImage(header.biWidth, header.biHeight);
    int32_t padding = img->width % 4;

    for (int32_t row = img->height - 1; row >= 0; row--) {

        size_t img_bytes_read = fread(&img->data[row*img->width], sizeof (struct pixel), img->width, in);

        //check if read less bytes than expected
        if(img_bytes_read != img->width){
            //free(&header);
            destroyImage(img);
            return READ_INVALID_BITS;
        }
        //check if we can move to the next row
        if (fseek(in, padding, SEEK_CUR)) {
            //free(&header);
            destroyImage(img);
            return READ_INVALID_BITS;
        }
    }

    //free(&header);
    return READ_OK;
}

struct bmp_header createValidBMPHeaderFromImage(const struct image* image){
    return (struct bmp_header) {
            .bfType = 0x4d42,
            .bOffBits = 54,
            .biSize = 40,
            .biPlanes = 1,
            .biCompression = 0,
            .biBitCount = 24,
            .biWidth = image->width,
            .biHeight = image->height,
            .biSizeImage = image->height * (image->width * sizeof(struct pixel) + image->width % 4 ),
            .bfileSize = 54 + image->height * (image->width * sizeof(struct pixel) + image->width % 4 )
    };
}

enum writeStatus toBmp(FILE* out, struct image const* img ){
    //array for padding values
    const uint8_t padding_arr[] = {0,0,0};
    //creating our valid bmp header
    struct bmp_header header = createValidBMPHeaderFromImage(img);

    //writing the header n checking whether we wrote it
    if(fwrite(&header, sizeof (struct bmp_header), 1, out) < 1){
        //free(&header);
        return WRITE_ERROR;
    }

    int32_t padding = img->width % 4;

    //writing the image
    for (int32_t row = img->height - 1; row >= 0; --row) {

        //check if wrote less bytes than expected
        if(fwrite(&img->data[row*img->width], sizeof (struct pixel), img->width, out) != img->width){
            //free(&header);
            return WRITE_ERROR;
        }
        //check if we can move to the next row
        if (fwrite(padding_arr, 1, padding, out) != padding) {
            //free(&header);
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;

}