#include "bmp_tools.h"
#include "image_rot.h"
//#include "utils.h"

int main(int argc, char** argv) {
    printf("\nStarting the bmp image rotation program\n");
    if (argc < 2){
        printf("No filename provided\n");
        printf("Usage: rotation [bmp_file]\n");
        return 0;
    }
    const char* const filename_in = argv[1];
    FILE* file_in = fopen( filename_in, "rb" );

    if(!file_in){
        exitBecauseOfError(openCloseError[OPEN_ERROR]);
    }

    struct image image = {0};

    printf("Reading file...\n");
    enum readStatus readStatus = fromBmp(file_in, &image);
    fclose(file_in);

    if (readStatus != READ_OK){
        exitBecauseOfError(readErrorsMessages[readStatus]);
    }
    printf("Trying to rotate the picture...\n");
    struct image res = rotateImage(image);

    FILE* file_out = fopen( "result.bmp", "wb" );
    if(!file_out){
        exitBecauseOfError(openCloseError[OPEN_ERROR]);
    }
    printf("Saving the result...\n");
    enum writeStatus writeStatus = toBmp(file_out, &res);
    fclose(file_out);
    if(writeStatus == WRITE_ERROR){
        exitBecauseOfError(writeErrorMessages[WRITE_ERROR]);
    }

    destroyImage(&image);
    destroyImage(&res);
    printf("Result successfully saved to Result.bmp\n");
    
    return 0;
}